var http = require('http');
var request = require('request');
var mongodb = require("mongodb");
var express = require('express');


var api = 'https://api.datamarket.azure.com/Bing/Search/v1/Image';

var username = "",
    password = process.env.API_KEY;

var db;
var app = express();

app.set('view engine', 'jade');
app.set('views', './views')

mongodb.MongoClient.connect(process.env.MONGOLAB_URI || "mongodb://localhost:27017", function (err, database) {
    if (err) {
        console.log(err);
        process.exit(1);
    }
    db = database;
    var server = app.listen(process.env.PORT || 8080, function () {
        var port = server.address().port;
        console.log("App now running on port", port);
    });
});

app.get("/imagesearch/", function (req, res) {
    request({
        url: api,
        auth: {
            user: username,
            password: password
        },
        qs: {
            "Query": "'" + req.query.q + "'",
            "$format": "json",
            "$top": "10",
            "$skip": req.query.offset || "0"
        }
    }, function (error, response, body) {
        var results = JSON.parse(body).d.results;
        results = results.map(function(item) {
            return {
                title: item["Title"],
                url: item["MediaUrl"],
                context: item["SourceUrl"]
            };
        });
        res.json(results);
        var newRequest = {
            term: req.query.q,
            offset: Number(req.query.offset) || 0,
            when: new Date()
        };
        db.collection("recent").insertOne(newRequest, function (err, doc) {
            if (err) {
                handleError(res, err.message, "Failed to log new request.");
            } else {
                console.log(newRequest);
            }
        });
    });
});

app.get("/latest", function (request, response) {
    db.collection("recent").find(null, {_id: false}).sort({when: -1}).limit(10).toArray(function (err, doc) {
        if (err) {
            handleError(res, err.message, "Failed to get contact");
        } else {
            response.json(doc);
        }
    });
});


app.get("/favicon.ico", function (request, response) {
    response.end();
});
app.get("/", function (request, response) {
    response.render('index', { host: request.headers.hostname || request.headers.host});
});

function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({"error": message});
}
